# Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).

Mis notas:

Started GET "/static_pages/help" for 187.176.28.153 at 2017-02-10 18:33:31 +0000
Cannot render console from 187.176.28.153! Allowed networks: 127.0.0.1, ::1, 127.0.0.0/127.255.255.255
Processing by StaticPagesController#help as HTML
1: hola...josue-action
  Rendering static_pages/help.html.erb within layouts/application
2: entra al helper html erb
  Rendered static_pages/help.html.erb within layouts/application (2.2ms)
3: hola josue-application-template-antes-de-full_title
4: entra al full_title
5: hola josue-application-template-despues-de-full_title

finalmente inserta con yield.
Completed 200 OK in 240ms (Views: 226.6ms | ActiveRecord: 0.0ms)


https://ruby-doc.org/docs/ruby-doc-bundle/Manual/man-1.4/syntax.html