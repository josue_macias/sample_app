module ApplicationHelper
  # Returns the full title on a per-page basis.
  #we’ll define a custom helper called full_title:
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end

#It’s convenient to use the full_title helper in the tests by including the Application helper into the test helper.