require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup #this automatically runs before every test
    @base_title = "Ruby on Rails Tutorial Sample App"
  end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "#{@base_title}" #the code checks for the presence of a <title> tag containing the string “Home | Ruby on Rails Tutorial Sample App”.
  end
=begin
The test method takes in a string argument (the description) and a block, a
nd then executes the body of the block as part of running the test suite.
=end
  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | #{@base_title}"
  end
  
  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"
  end
  
  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end
  
end
=begin
get static_pages_*_url
assert_response :success

Here the use of get indicates that our tests expect the Home page to be ordinary web page, 
accessed using a GET request (Box 3.2). The response :success is an abstract representation of 
the underlying HTTP status code (in this case, 200 OK).
In other words, it says: 
“Let’s test the Home page by issuing a GET request to the Static Pages home URL and then making sure we receive a ‘success’ status code in response.”


=end