require 'test_helper' #incluye el archivo test_helper.rb

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "layout links" do
    get root_path
    assert_template 'static_pages/home' #method to verify that the Home page is rendered using the correct view.
    assert_select 'a[href=?]', root_path, count: 2
    assert_select 'a[href=?]', help_path
    assert_select 'a[href=?]', about_path
    assert_select 'a[href=?]', contact_path
    get contact_path
    assert_select "title", full_title("Contact")
    # In this case, we use a syntax that allows us to test for the presence of a particular link–URL combination by specifying the tag name a and attribute href.
  end
  
  test "get and full_title" do
    get signup_path
    assert_select "title", full_title("Sign up")
  end
end


=begin

we’ll simulate the same series of steps using an integration test, 
which allows us to write an end-to-end test of our application’s behavior. 
We can get started by generating a template test, which we’ll call site_layout.

Note that the Rails generator automatically appends _test to the name of the test file.

With the added integration test for layout links, we are now in a good position to catch regressions quickly using our test suite.

=end