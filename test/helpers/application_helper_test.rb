require 'test_helper'
# creating a file to test the application helper and then filling in the code indicated with FILL_IN
class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Ruby on Rails Tutorial Sample App"
    assert_equal full_title("Help"), "Help | Ruby on Rails Tutorial Sample App"
  end
end

=begin
It’s convenient to use the full_title helper in the tests by including the Application helper into the test helper.

=end