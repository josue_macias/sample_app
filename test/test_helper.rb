ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper #It’s convenient to use the full_title helper in the tests by including the Application helper into the test helper.
  # Add more helper methods to be used by all tests here...
end

#ApplicationHelper: is a module (file: app/helpers/application_helper.rb) where we've defined our custom helpers.