Rails.application.routes.draw do
  root 'static_pages#home' # aquí tuve duda sobre comentar ésta linea para obtener un red en testing: https://www.railstutorial.org/book/static_pages#code-home_root_route_commented_out
  #get 'static_pages/home'
  get '/help', to: 'static_pages#help' #get 'static_pages/help'
  get '/about', to: 'static_pages#about' #get 'static_pages/about'
  get '/contact', to: 'static_pages#contact' #get 'static_pages/contact'
  get '/signup', to: 'users#new' #get 'users/new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
